(function() {
  'use strict';

  angular
    .module('frontend', ['ngMessages', 'ui.router', 'toastr']);

})();
