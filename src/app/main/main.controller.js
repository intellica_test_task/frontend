(function () {
  'use strict';

  angular
    .module('frontend')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(HttpService, $log, $document) {
    var vm = this;
    vm.dialogShow = false;
    vm.editedName = '';
    vm.editedRole = '';
    vm.editedId = -1;

    HttpService.getAllUsers()
      .then(function (data) {
        vm.usersList = data.data;
      })
      .catch($log.debug);

    vm.removeUser = function (id) {
      HttpService.removeUser(id)
        .then(function() {
          $document[0].querySelector("#tr" + id).remove();
          for (var i = 0; i < vm.usersList.length; i++) {
            if (vm.usersList[i].id == id) {
              vm.usersList.splice(i,1);
            }
          }
        })
        .catch($log.debug);
    }

    vm.showModal = function (id) {
      vm.editedId = id;
      for (var i = 0; i < vm.usersList.length; i++) {
        if (vm.usersList[i].id == id) {
          vm.editedName = id > 0 ? vm.usersList[i].name : '';
          vm.editedRole = id > 0 ? vm.usersList[i].role : '';
          break;
        }
      }
      vm.dialogShow = !vm.dialogShow;
    }

    vm.saveUser = function () {
      if (vm.editedId != -1) {
        for (var i = 0; i < vm.usersList.length; i++) {
          if (vm.usersList[i].id == vm.editedId) {
            vm.editedName ? vm.usersList[i].name = vm.editedName : {};
            vm.editedRole ? vm.usersList[i].role = vm.editedRole : {};
            break;
          }
        }
        HttpService.updateUser(vm.editedId, vm.editedName, vm.editedRole)
          .then(function (response) {
            if (response) {
              vm.dialogShow = false;
              vm.editedName = null;
              vm.editedRole = null;
              vm.editedId = null;
            }
          })
      }
      else {
        HttpService.createUser(vm.editedName, vm.editedRole)
          .then(function (data) {
            vm.editedName = null;
            vm.editedRole = null;
            vm.editedId = null;
            vm.dialogShow = false;
            if (data.data instanceof Object) {
              vm.usersList.push(data.data);
            }
            else console.log(data.data);
          })
      }
    }
  }
})();
