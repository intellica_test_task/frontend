(function() {
  'use strict';

  angular
    .module('frontend')
    .service('HttpService', HttpService);

    function HttpService($http) {
      var self = this;
      self.getAllUsers = getAllUsers;
      self.removeUser = removeUser;
      self.updateUser = updateUser;
      self.createUser = createUser;

      function getAllUsers() {
        return $http.get('http://localhost:3000/users/get');
      }

      function removeUser(id) {
        return $http({
          url: 'http://localhost:3000/users/remove',
          method: 'DELETE',
          data: {userId: id},
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          }
        })
      }

      function updateUser(id, newName, newRole) {
        var payload = {
          userId: id
        };
          payload.name = newName ? newName : null;
          payload.role = newRole ? newRole : null;

        return $http({
          url: 'http://localhost:3000/users/update',
          method: 'PUT',
          data: payload,
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          }
        })
      }

      function createUser(newName, newRole) {
        var payload = {};
        payload.name = newName ? newName : null;
        payload.role = newRole ? newRole : null;

        return $http({
          url: 'http://localhost:3000/users/add',
          method: 'POST',
          data: payload,
          headers: {
            "Content-Type": "application/json;charset=utf-8"
          }
        })
      }

    }
})();
