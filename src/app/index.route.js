(function() {
  'use strict';

  angular
    .module('frontend')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/index',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'MainCtrl'
      })

    $urlRouterProvider.otherwise('/index');
  }

})();
