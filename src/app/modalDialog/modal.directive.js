(function () {
  'use strict';

  angular
    .module('frontend')
    .directive('modalDialog', ModalDialog);

  /** @ngInject */

function ModalDialog() {
      return {
        restrict: 'E',
        scope: {
          show: '=',
          name: '=',
          role: '=',
          id: '=',
          save: '='
        },
        replace: true,
        transclude: true,
        link: function(scope) {
          scope.hideModal = function() {
            scope.show = false;
          };
        },
        templateUrl: 'app/modalDialog/modal.html'
      };
  }

})();
